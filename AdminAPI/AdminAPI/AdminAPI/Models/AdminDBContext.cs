﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace AdminAPI.Models
{
    public class AdminDBContext:DbContext
    {
        public AdminDBContext() : base("FoodEntities")
        {

        }

        public DbSet<Admin> Admins { get; set; }
    }
}