﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AdminAPI.Models
{
    public class Admin
    {
        [Key]
        public int AdminID { get; set; }
        [Required(ErrorMessage = "Username is required")]
        [Display(Name = "Username")]
        [MinLength(5, ErrorMessage = "Minimum 5 characters required"), MaxLength(8, ErrorMessage = "Maximum 8 characters allowed")]
        public string Admin_Username { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [MinLength(8, ErrorMessage = "Minimum 8 characters required"), MaxLength(12, ErrorMessage = "Maximum 12 characters allowed")]
        public string Password { get; set; }



        public string Status { get; set; }
    }
}