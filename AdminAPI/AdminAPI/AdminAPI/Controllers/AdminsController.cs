﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AdminAPI.Models;

namespace AdminAPI.Controllers
{
    public class AdminsController : ApiController
    {
        private AdminDBContext db = new AdminDBContext();

        // GET: api/Admins
        public IQueryable<Admin> GetAdmins()
        {
            return db.Admins;
        }

        // GET: api/Admins/5
        [ResponseType(typeof(Admin))]
        public IHttpActionResult GetAdmin(int id)
        {
            Admin admin = db.Admins.Find(id);
            if (admin == null)
            {
                return NotFound();
            }

            return Ok(admin);
        }

        // PUT: api/Admins/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAdmin( Admin admin)
        {
            /*  if (!ModelState.IsValid)
              {
                  return BadRequest(ModelState);
              }

              if (id != admin.AdminID)
              {
                  return BadRequest();
              }

              db.Entry(admin).State = EntityState.Modified;

              try
              {
                  db.SaveChanges();
              }
              catch (DbUpdateConcurrencyException)
              {
                  if (!AdminExists(id))
                  {
                      return NotFound();
                  }
                  else
                  {
                      throw;
                  }
              }

              return StatusCode(HttpStatusCode.NoContent);*/
            try
            {
                var id = 1;
                var entity = db.Admins.FirstOrDefault(s => s.AdminID == id);
                if (entity == null)
                   
                {
                    return (IHttpActionResult)Request.CreateResponse(HttpStatusCode.NotFound, $"Admin with AdminID {id} not found to update");
                }
                else
                {
                    entity.Admin_Username = admin.Admin_Username;
                    entity.Password = admin.Password;
                    entity.Status = admin.Status;
                    db.SaveChanges();
                    return (IHttpActionResult)Request.CreateResponse(HttpStatusCode.OK, entity);
                }
            }
            catch (Exception ex)
            {
                return (IHttpActionResult)Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // POST: api/Admins
        [ResponseType(typeof(Admin))]
        public IHttpActionResult PostAdmin(Admin admin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Admins.Add(admin);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = admin.AdminID }, admin);
        }

        // DELETE: api/Admins/5
        [ResponseType(typeof(Admin))]
        public IHttpActionResult DeleteAdmin(int id)
        {
            Admin admin = db.Admins.Find(id);
            if (admin == null)
            {
                return NotFound();
            }

            db.Admins.Remove(admin);
            db.SaveChanges();

            return Ok(admin);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AdminExists(int id)
        {
            return db.Admins.Count(e => e.AdminID == id) > 0;
        }
    }
}